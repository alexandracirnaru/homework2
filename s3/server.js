const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

app.put('/putProd:idProd', (request, response) => {
    let ok=false;
    
    products.forEach((product)=>{
       if(product.id==request.params.idProd)
       {
           product.id=request.body.id;
           product.price=request.body.price;
           product.productName=request.body.productName;
           console.log('Modificarile au fost facute!');
           ok=true;
       }
    });
    
    if(ok==true)
    {
        response.status(200).send("Modificarile au fost facute!");
    }
        else
        {
            return response.status(500).send("Produsul nu a fost gasit!");
        }
    
});

app.delete('/deleteProd',(request,response)=>{
    const nameProd=request.body.productName;
    let idProd=-1;
    
    products.forEach((product)=>{
        if(product.productName==nameProd)
        {
            idProd=product.id;
        }
    });
    
    if(idProd==-1)
    {
        console.log('Stergerea nu s-a efectuat!');
        return response.status(500).send('Produsul nu a fost gasit!');
    }
    
    else
    {
        products.splice(idProd,1);
        console.log('Stergerea s-a efectuat!');
        response.status(200).send('Done!');
        
    }
});



app.listen(8080, () => {
    console.log('Server started on port 8080...');
});